package com.aovill.nhk.news.batch.object;

import java.io.IOException;

import com.aovill.db.AObject;
import com.aovill.db.annotation.Column;
import com.aovill.db.annotation.Table;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.helper.StringHelper;
import com.aovill.nhk.news.batch.Config;
import com.aovill.nhk.news.batch.common.Constants;
import com.aovill.nhk.news.batch.logger.Logger;

@Table(name = "aou_nhk_news_articles")
public class Article extends AObject {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// news id:k10010005141000
	@Column(name = "code", isPrimaryKey = true)
	public String code;

	@Column(name = "categories")
	public String categories;

	@Column(name = "title")
	public String title;

	// title with ruby
	@Column(name = "title_with_ruby")
	public String titleWithRuby;

	// news URL
	@Column(name = "url")
	public String url;

	@Column(name = "url_movie")
	public String movieUrl;

	@Column(name = "url_image")
	public String imgUrl;

	@Column(name = "url_image_downloaded")
	public String imgUrlDownloaded;

	@Column(name = "excerpt")
	public String excerpt;

	@Column(name = "excerpt_with_ruby")
	public String excerptWithRuby;

	// content with ruby
	@Column(name = "content")
	public String content;

	@Column(name = "content_with_ruby")
	public String contentWithRuby;

	@Column(name = "target_date")
	public String targetDate;

	@Column(name = "published_dt")
	public String publishedDate;

	@Column(name = "url_movie_downloaded")
	public String movieUrlDownloaded;

	@Column(name = "related_articles")
	public String relatedArticles;

	@Column(name = "view", type = Integer.class)
	public Integer view = 0;

	@Column(name = "is_favorite", type = Integer.class)
	public Integer isFavorite = 0;

	@Column(name = "created_dt")
	public String createdDt;

	@Column(name = "updated_dt")
	public String updatedDt;

	public boolean isImgExistOnline = true;

	public String toString() {
		return code;
	}

	public String getImg() {
		String result;
		if (StringHelper.isEmpty(imgUrl) || imgUrl.startsWith("http")) {
			result = imgUrl;
		} else if (imgUrl.contains("nhk.or.jp")) {
			result = "http:" + imgUrl;
		} else {
			result = Constants.NEWS_HOME + "/news/" + imgUrl;
		}
		return result;
	}

	public boolean isMetaDifferent(Article tmp) {
		return !StringHelper.equalsIgnoreCase(this.title, tmp.title)
				|| !StringHelper.equalsIgnoreCase(this.imgUrl, tmp.imgUrl)
				|| !StringHelper.equalsIgnoreCase(this.movieUrl, tmp.movieUrl);
	}

	public boolean isDifferent(Article tmp) {
		return isMetaDifferent(tmp) || !StringHelper.equalsIgnoreCase(this.content, tmp.content);
	}

	/**
	 * Get table name from Config file, if N/A then get from Annotation
	 */
	@Override
	public String getTableName() {
		String property = Config.DB_MAPPING + this.getClass().getSimpleName();
		try {
			return Config.getInstance().getProperty(property);
		} catch (NotFoundPropertiesFile | IOException e) {
			Logger.error(e);
		}
		return super.getTableName();
	}
}
