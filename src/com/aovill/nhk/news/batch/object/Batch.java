package com.aovill.nhk.news.batch.object;

import java.io.IOException;

import com.aovill.db.AObject;
import com.aovill.db.annotation.Column;
import com.aovill.db.annotation.Table;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.nhk.news.batch.Config;
import com.aovill.nhk.news.batch.logger.Logger;

@Table(name = "batch_execution")
public class Batch extends AObject {

	public final static Integer STATUS_NOT_RUNNING = 0;
	public final static Integer STATUS_RUNNING = 1;
	public final static Integer STATUS_ERROR = 9;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "batch_id", isPrimaryKey = true)
	public String batchId;

	// 0: not running, 1: running, 9: error
	@Column(name = "status")
	public Integer status;

	@Column(name = "last_started_dt")
	public String lastStartedDt;

	@Column(name = "last_completed_dt")
	public String lastCompletedDt;

	@Column(name = "max_waiting_mins")
	public Integer maxWaitingMins;

	public String toString() {
		return batchId;
	}

	@Override
	public String getTableName() {
		String property = Config.DB_MAPPING + this.getClass().getSimpleName();
		try {
			return Config.getInstance().getProperty(property);
		} catch (NotFoundPropertiesFile | IOException e) {
			Logger.error(e);
		}
		return super.getTableName();
	}
}
