package com.aovill.nhk.news.batch.dao;

import java.io.IOException;

import com.aovill.db.exception.NonSupportedDBException;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.nhk.news.batch.object.Batch;

public class BatchDao extends BatchCommonDao<Batch> {

	public BatchDao() throws NonSupportedDBException, NotFoundPropertiesFile, IOException {
		super();
	}

	public Batch selectById(final String batchId) {
		Batch batch = new Batch();
		batch.batchId = batchId;
		return selectById(batch);
	}
}
