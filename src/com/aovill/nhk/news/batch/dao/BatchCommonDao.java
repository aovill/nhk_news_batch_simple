package com.aovill.nhk.news.batch.dao;

import java.io.IOException;

import com.aovill.db.AObject;
import com.aovill.db.CommonDao;
import com.aovill.db.exception.NonSupportedDBException;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.nhk.news.batch.Config;

public abstract class BatchCommonDao<T extends AObject> extends CommonDao<T> {

	public BatchCommonDao() throws NonSupportedDBException, NotFoundPropertiesFile, IOException {
		super(Config.getInstance().getConnectionString(), "", Config.getInstance().getDbUsername(),
				Config.getInstance().getDbPassword());
	}
}
