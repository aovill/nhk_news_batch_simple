package com.aovill.nhk.news.batch.dao;

import java.io.IOException;
import java.util.List;

import com.aovill.db.exception.NonSupportedDBException;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.nhk.news.batch.object.Article;

public class ArticleDao extends BatchCommonDao<Article> {

	public ArticleDao() throws NonSupportedDBException, NotFoundPropertiesFile, IOException {
		super();
	}

	public Article selectById(final String code) {
		Article article = new Article();
		article.code = code;
		return selectById(article);
	}

	public List<Article> selectImcompletedArticle() {
		String query = String.format("SELECT * FROM %s WHERE " + "title_with_ruby IS NULL " + "OR content IS NULL "
				+ "OR content_with_ruby IS NULL LIMIT 5", new Article().getTableName());
		return selectList(query);
	}
}
