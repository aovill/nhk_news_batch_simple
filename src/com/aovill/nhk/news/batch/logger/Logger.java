package com.aovill.nhk.news.batch.logger;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by Hoang Phu on 1/11/2015.
 */
public class Logger {
	public static final int STACK_DEPTH = 5;
	public static final String LOG_MESSAGE_TEMPLATE = "{loglevel}-{class}-{method}-{message}-{custom}";
	public static final String LOG_TAG = "Aovill Logger";

	public static final String LOG_LEVEL_DEBUG = "DEBUG";
	public static final String LOG_LEVEL_INFO = "INFO";
	public static final String LOG_LEVEL_ERROR = "ERROR";
	public static final String LOG_LEVEL_WARN = "WARN";

	public static final String LOG_METHOD_START = "START";
	public static final String LOG_METHOD_END = "END";

	private static void log(String tag, String message) {
		log(tag, message, null);
	}

	private static void log(String tag, String message, Exception e) {
		System.out.println(tag + " - " + message);
		if (e != null) {
			e.printStackTrace();
		}
	}

	public static String start(String message) {
		String messageLog = generateMessage(LOG_METHOD_START, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String end(String message) {
		String messageLog = generateMessage(LOG_METHOD_END, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String debug(String message) {
		String messageLog = generateMessage(LOG_LEVEL_DEBUG, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String info(String message) {
		String messageLog = generateMessage(LOG_LEVEL_INFO, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String error(String message) {
		String messageLog = generateMessage(LOG_LEVEL_ERROR, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String warn(String message) {
		String messageLog = generateMessage(LOG_LEVEL_WARN, message);
		log(LOG_TAG, messageLog);
		return messageLog;
	}

	public static String debug(String message, Exception e) {
		String messageLog = generateMessage(LOG_LEVEL_DEBUG, message);
		log(LOG_TAG, messageLog, e);
		return messageLog;
	}

	public static String info(String message, Exception e) {
		String messageLog = generateMessage(LOG_LEVEL_INFO, message);
		log(LOG_TAG, messageLog, e);
		return messageLog;
	}

	public static String error(String message, Exception e) {
		String messageLog = generateMessage(LOG_LEVEL_ERROR, message);
		log(LOG_TAG, messageLog, e);
		return messageLog;
	}

	public static String error(Exception e) {
		return error(e.getMessage(), e);
	}

	public static String warn(String message, Exception e) {
		String messageLog = generateMessage(LOG_LEVEL_WARN, message);
		log(LOG_TAG, messageLog, e);
		return messageLog;
	}

	private static String generateMessage(String logLevel, String message) {
		return generateMessage(logLevel, message, null);
	}

	private static String generateMessage(String logLevel, String message, String customMessage) {
		String messageTemp = LOG_MESSAGE_TEMPLATE;
		int depth = Thread.currentThread().getStackTrace().length - 1;
		if (depth >= STACK_DEPTH) {
			depth = STACK_DEPTH;
		}
		StackTraceElement stack = Thread.currentThread().getStackTrace()[depth];

		if (StringUtils.isNotEmpty(logLevel)) {
			messageTemp = messageTemp.replace("{loglevel}", logLevel);
		}
		if (stack != null) {
			messageTemp = messageTemp.replace("{class}", stack.getClassName());
			messageTemp = messageTemp.replace("{method}", stack.getMethodName());
		}
		if (StringUtils.isNotEmpty(message)) {
			messageTemp = messageTemp.replace("{message}", message);
		}
		if (StringUtils.isNotEmpty(customMessage)) {
			messageTemp = messageTemp.replace("{custom}", customMessage);
		}
		return messageTemp;
	}
}
