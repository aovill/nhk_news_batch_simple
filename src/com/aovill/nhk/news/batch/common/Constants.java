package com.aovill.nhk.news.batch.common;

import java.util.ArrayList;
import java.util.List;

public class Constants {

	public static final String APP_KEY = "nhk_news_batch";

	public static final String NEWS_HOME = "http://www3.nhk.or.jp";

	public static final int FILE_COUNT_TO_GET = 10;

	public static final String TOP_URL = "http://www3.nhk.or.jp/news/json16/new_%s.json";

	public static final String CATEGORY_URL = "http://www3.nhk.or.jp/news/json16/cat0%s_%s.json";

	public static List<String> API_SERVERS_GET_RUBY_CONTENT;

	static {
		API_SERVERS_GET_RUBY_CONTENT = new ArrayList<>();
		API_SERVERS_GET_RUBY_CONTENT.add("http://s1.aovill.com/nhk/getArticle.php?c=%s&k=%s&ti=%s&reader2");
		API_SERVERS_GET_RUBY_CONTENT.add("http://news.aovill.com/nhk/getArticle.php?c=%s&k=%s&ti=%s");
	}

	public static String BATCH_ID = "nhk_news_batch";

	public static Integer MAX_WAITING_MINS = 60;
}
