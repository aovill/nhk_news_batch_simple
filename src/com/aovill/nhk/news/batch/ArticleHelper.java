package com.aovill.nhk.news.batch;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.helper.DateHelper;
import com.aovill.helper.DownloadHelper;
import com.aovill.helper.FileHelper;
import com.aovill.helper.JapaneseHelper;
import com.aovill.helper.StringHelper;
import com.aovill.json.JSONArray;
import com.aovill.json.JSONException;
import com.aovill.json.JSONObject;
import com.aovill.nhk.news.batch.common.Constants;
import com.aovill.nhk.news.batch.dao.ArticleDao;
import com.aovill.nhk.news.batch.logger.Logger;
import com.aovill.nhk.news.batch.object.Article;

/**
 * Created by HoangPhu on 2018/06/23.
 */
public class ArticleHelper {

	public static List<String> getUrls() throws NotFoundPropertiesFile, IOException {
		List<String> urls = new ArrayList<String>();
		for (int i = 1; i <= Config.getInstance().getFindCountToGet(); i++) {
			String index = "00" + i;
			urls.add(String.format(Constants.TOP_URL, index));
			for (int cate = 1; cate <= 8; cate++) {
				urls.add(String.format(Constants.CATEGORY_URL, cate, index));
			}
		}
		return urls;
	}

	public static boolean loadArticleListData(ArticleDao articleDao)
			throws NotFoundPropertiesFile, JSONException, IOException {
		URL url;
		StringBuilder articleListData;
		Scanner scan;
		for (String urlStr : getUrls()) {
			articleListData = new StringBuilder();
			articleListData = new StringBuilder();
			Logger.debug("############################################");
			Logger.debug("Start load file: " + urlStr);
			try {
				url = new URL(urlStr);
				scan = new Scanner(url.openStream());
				while (scan.hasNext()) {
					articleListData.append(scan.nextLine());
				}
				scan.close();
				JSONObject obj = new JSONObject(articleListData.toString());
				Article article, articleDb;
				JSONArray elements = obj.getJSONObject("channel").getJSONArray("item");
				for (int i = 0; i < elements.length(); i++) {
					Logger.debug("=============================");
					article = ArticleHelper.createArticle(urlStr, elements.getJSONObject(i));
					Logger.debug("Loading article: " + article.code + " - " + article.title);
					articleDb = articleDao.selectById(article.code);

					loadDetailData(article, articleDb);
					if (articleDb == null) {
						Logger.debug("Start inserting article to DB: " + article.code + " - " + article.title);
						article.createdDt = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ);
						articleDao.insert(article);
					} else if (article.isDifferent(articleDb)) {
						Logger.debug("Start updating article: " + article.code + " - " + article.title);
						article.updatedDt = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ);
						articleDao.update(article);
					}

					articleDao.commit();
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//articleDao.commit();
			//articleDao.close();
			//articleDao.start();
		}
		return true;
	}

	public static void reloadImcompletedData(ArticleDao articleDao) throws NotFoundPropertiesFile {
		Logger.start(" Refill data");

		List<Article> list = articleDao.selectImcompletedArticle();
		while (list.size() > 0) {
			for (Article article : list) {
				loadDetailData(article, null);
				Logger.debug("Start updating article: " + article.code + " - " + article.title);
				articleDao.update(article);
				articleDao.commit();
			}
			list = articleDao.selectImcompletedArticle();
		}
		Logger.end(" Refill data");
	}

	/**
	 * Load article detail data
	 *
	 * @param article
	 *            Article
	 * @throws NotFoundPropertiesFile
	 */
	public static void loadDetailData(Article article, Article articleDb) throws NotFoundPropertiesFile {
		Document document;
		StringBuilder content;
		Logger.debug("Loading article detail: " + article.code + " - " + article.title);
		try {
			document = Jsoup.connect(getLink(article.url)).get();
			Element eleContent = document.getElementById("news_textbody");
			Element eleContent2 = document.getElementById("news_textmore");
			Elements eleContentAdd = document.getElementsByClass("news_add");
			// Element eleTime = document.getElementById("newsDate");
			content = new StringBuilder();
			if (eleContent != null)
				content.append(eleContent.html());

			if (eleContent2 != null) {
				content.append("</br></br>");
				content.append(eleContent2.html());
				for (Element ele : eleContentAdd) {
					content.append(ele.html());
				}
			}

			if (articleDb == null || StringHelper.isEmpty(articleDb.titleWithRuby) || articleDb != null
					&& StringHelper.isNotEmpty(article.title) && !article.title.equalsIgnoreCase(articleDb.title)) {
				article.titleWithRuby = JapaneseHelper.getFuriganaHtml(article.title);
			}
			if (StringHelper.isNotEmpty(content)) {
				// article.content = content.toString().replaceAll("<img ", "<img
				// width=\"100%\"");
				article.content = content.toString();
			}

			if (articleDb == null || StringHelper.isEmpty(articleDb.contentWithRuby)
					|| articleDb != null && !article.content.equalsIgnoreCase(articleDb.content)) {
				article.contentWithRuby = JapaneseHelper.getFuriganaHtml(article.content);
			}

			// if (eleTime != null && StringUtils.isEmpty(article.publishedDate)) {
			// article.publishedDate = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ,
			// DateHelper.getDate(DateHelper.YYYYMMDDHHmmssFull, eleTime.text()));
			// }

			// If this is batch load data for multiple article then update
			Config config = Config.getInstance();
			if (StringUtils.isNotEmpty(article.getImg())) {
				article.imgUrlDownloaded = article.getImg().substring(article.getImg().lastIndexOf("/"));
				String saveImage = config.getImageDir() + article.imgUrlDownloaded;
				if (!FileHelper.checkFileExistence(saveImage)) {
					boolean downloadResult = DownloadHelper.download(article.getImg(), saveImage);
					if (downloadResult) {
						Logger.debug("Download finished: " + config.getImageDir() + article.imgUrlDownloaded);
					} else {
						Logger.debug("Download failed: " + config.getImageDir() + article.imgUrlDownloaded);
					}
				} else {
					Logger.debug(" Already downloaded: " + saveImage);
				}
			}

		} catch (IOException | JSONException e) {
			Logger.error(e.getMessage(), e);
		}
	}

	/**
	 * Create article from JSON object
	 *
	 * @param category
	 *            Category
	 * @param element
	 *            JSON object
	 * @return article
	 */
	private static Article createArticle(String url, JSONObject element) {
		Article article = new Article();
		String link = element.getString("link");
		article.url = getLink(link);
		article.code = article.url.substring(article.url.lastIndexOf("/") + 1, article.url.lastIndexOf("."));
		article.title = element.getString("title");

		String cate = element.getJSONArray("cate_group").join("");
		if (url.contains("new_")) {
			cate = "\"0\"" + cate;
		}
		article.categories = cate;
		article.movieUrl = element.getString("videoPath");
		article.publishedDate = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ,
				DateHelper.getDate(DateHelper.EEEDDMMMYYYYHHmmss, element.getString("pubDate")));
		article.imgUrl = element.getString("imgPath").trim();
		article.targetDate = link.substring(link.indexOf("/") + 1, link.lastIndexOf("/"));

		JSONArray relatedArticles = element.getJSONArray("relationNews");
		String tmpLink;
		StringBuilder buider = new StringBuilder();
		for (int i = 0; i < relatedArticles.length(); i++) {
			tmpLink = relatedArticles.getJSONObject(i).getString("link");
			buider.append(",").append(tmpLink.substring(tmpLink.lastIndexOf("/") + 1, tmpLink.lastIndexOf(".")));
		}
		article.relatedArticles = buider.toString();

		return article;
	}

	private static String getLink(String link) {
		if (StringUtils.isEmpty(link)) {
			return link;
		}
		String result;
		if (link.startsWith("http")) {
			result = link;
		} else {
			result = Constants.NEWS_HOME + "/news/" + link;
		}
		Logger.debug(result);
		return result;
	}
}
