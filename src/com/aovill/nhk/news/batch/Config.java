package com.aovill.nhk.news.batch;

import java.io.IOException;
import java.util.Properties;

import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.helper.ConfigHelper;
import com.aovill.helper.StringHelper;
import com.aovill.nhk.news.batch.common.Constants;
import com.aovill.nhk.news.batch.logger.Logger;

public class Config {
	public static final String CONFIG_FILE = "nhk_news_batch.properties";

	private static final String DB_CONNECTION_STRING = "db.connection_string";
	private static final String DB_USERNAME = "db.username";
	private static final String DB_PASSWORD = "db.password";

	public static final String DB_MAPPING = "db.mapping.";

	private static final String IMAGE_DIR = "dir.image_home";

	private static final String FILE_COUNT_TO_GET = "app.file_count_to_get";

	private static final String BATCH_NAME = "app.batch_name";

	public Properties properties;

	public static Config config;

	public Config(String file) throws NotFoundPropertiesFile, IOException {
		properties = ConfigHelper.loadProperties(file);
	}

	public synchronized static Config getInstance() throws NotFoundPropertiesFile, IOException {
		if (config == null || config.properties == null) {
			config = new Config(CONFIG_FILE);
		}
		return config;
	}

	public String getConnectionString() {
		return ConfigHelper.getProperty(properties, DB_CONNECTION_STRING);
	}

	public String getDbUsername() {
		return ConfigHelper.getProperty(properties, DB_USERNAME);
	}

	public String getDbPassword() {
		return ConfigHelper.getProperty(properties, DB_PASSWORD);
	}

	public String getImageDir() {
		return ConfigHelper.getProperty(properties, IMAGE_DIR);
	}

	public Integer getFindCountToGet() {
		int result = Constants.FILE_COUNT_TO_GET;
		try {
			result = Integer.parseInt(ConfigHelper.getProperty(properties, FILE_COUNT_TO_GET));
		} catch (Exception e) {
			Logger.error(e);
		}
		return result;
	}

	public String getBatchName() {
		String name = ConfigHelper.getProperty(properties, BATCH_NAME);
		if (StringHelper.isEmpty(name)) {
			name = Constants.BATCH_ID + "_" + getFindCountToGet();
		}
		return name;
	}

	public String getProperty(String propertyName) {
		return ConfigHelper.getProperty(properties, propertyName);
	}
}
