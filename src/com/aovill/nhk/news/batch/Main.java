package com.aovill.nhk.news.batch;

import java.io.IOException;
import java.util.Date;

import com.aovill.db.exception.NonSupportedDBException;
import com.aovill.exception.NotFoundPropertiesFile;
import com.aovill.helper.DateHelper;
import com.aovill.helper.StringHelper;
import com.aovill.nhk.news.batch.common.Constants;
import com.aovill.nhk.news.batch.dao.ArticleDao;
import com.aovill.nhk.news.batch.dao.BatchDao;
import com.aovill.nhk.news.batch.logger.Logger;
import com.aovill.nhk.news.batch.object.Batch;

public class Main {

	public static void main(String[] args) throws NonSupportedDBException, NotFoundPropertiesFile, IOException {
		Date start = new Date();
		Logger.start("Start: " + DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ, start));
		Config config = Config.getInstance();

		BatchDao batchDao = new BatchDao();
		batchDao.start();
		ArticleDao articleDao = new ArticleDao();
		articleDao.start();

		try {
			if (!checkBatchExecutable(batchDao, config.getBatchName())) {
				return;
			}
			ArticleHelper.loadArticleListData(articleDao);
			ArticleHelper.reloadImcompletedData(articleDao);
		} catch (Exception e) {
			Logger.error(e);
		}

		articleDao.commit();
		articleDao.close();

		Date end = new Date();
		String endStr = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ, end);
		Logger.start("Completed: " + DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ, end));
		Long runningTime = end.getTime() - start.getTime();
		Logger.start("Running Time : " + runningTime / 1000 + "." + runningTime % 1000);
		Batch batch = batchDao.selectById(config.getBatchName());
		if (batch != null) {
			batch.status = Batch.STATUS_NOT_RUNNING;
			batch.lastCompletedDt = endStr;
			batchDao.update(batch);
			batchDao.commit();
		}
		batchDao.close();
	}

	private static boolean checkBatchExecutable(BatchDao batchDao, String batchName)
			throws NonSupportedDBException, NotFoundPropertiesFile, IOException {
		batchDao.start();
		Batch batch = batchDao.selectById(batchName);
		Date now = new Date();
		boolean result;
		if (batch == null) {
			batch = new Batch();
			batch.batchId = batchName;
			batch.status = Batch.STATUS_RUNNING;
			batch.maxWaitingMins = Constants.MAX_WAITING_MINS;
			batch.lastStartedDt = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ, now);
			batchDao.insert(batch);
			result = true;
		} else {
			long completedTime = StringHelper.isEmpty(batch.lastCompletedDt) ? 0l
					: DateHelper.getDate(DateHelper.YYYYMMDDTHHMMSSZ, batch.lastCompletedDt).getTime();
			long waitedMins = (now.getTime() - completedTime) / 1000 / 60;
			if (batch.status != Batch.STATUS_RUNNING || waitedMins >= Constants.MAX_WAITING_MINS) {
				batch.status = Batch.STATUS_RUNNING;
				batch.lastStartedDt = DateHelper.getDateString(DateHelper.YYYYMMDDTHHMMSSZ, now);
				batchDao.update(batch);
				result = true;
			} else {
				result = false;
			}
		}
		batchDao.commit();
		return result;
	}
}
